import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_state_practice/store/app/app_state.dart';

class NavigationSelector {
  static void Function(String route, dynamic arguments) navigateTOAction(
      Store<AppState> store) {
    return (route, arguments) => store.dispatch(
          NavigateToAction.push(
            route,
            arguments: arguments,
          ),
        );
  }
}
