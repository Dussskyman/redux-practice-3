import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_state_practice/store/app/app_state.dart';

import 'navigation_selector.dart';

class NavigationVM {
  final void Function(String route, dynamic) changePage;

  NavigationVM({@required this.changePage});

  static NavigationVM fromStore(Store<AppState> store) {
    return NavigationVM(changePage: NavigationSelector.navigateTOAction(store));
  }
}
