import 'package:redux_epics/redux_epics.dart';
import 'package:redux_state_practice/repo/company_image_repo.dart';
import 'package:redux_state_practice/repo/company_repo.dart';
import 'package:redux_state_practice/store/app/app_state.dart';
import 'package:redux_state_practice/store/shared/main_page_state/main_page_actions.dart';
import 'package:rxdart/rxdart.dart';

class MainPageEpics {
  static final indexEpic = combineEpics([getCompanyByApi]);

  static Stream<dynamic> getCompanyByApi(
      Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<GetCompanyByApi>().switchMap(
      (action) async* {
        print('company');
        yield await CompanyRepo.instance
            .apiResponce()
            .then((value) => SetCompany(value));
        print('images');
        yield await CompanyImageRepo.instance
            .apiResponce()
            .then((value) => SetCompanyImage(value));
      },
    );
  }
}
