import 'package:redux_state_practice/models/company.dart';

class SetCompany {
  final List<Company> companies;

  SetCompany(this.companies);
}

class SetCompanyImage {
  final List<String> images;

  SetCompanyImage(this.images);
}

class GetCompanyByApi {}
