import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_state_practice/models/company.dart';
import 'package:redux_state_practice/store/app/app_state.dart';
import 'package:redux_state_practice/store/shared/main_page_state/main_page_selectors.dart';

class MainPageVM {
  List<Company> companies;
  List<String> images;
  final void Function() getCompanyByApi;

  MainPageVM({
    @required this.images,
    @required this.companies,
    this.getCompanyByApi,
  });

  static MainPageVM fromStore(Store<AppState> store) {
    return MainPageVM(
      companies: MainPageSelector.getCompany(store),
      images: MainPageSelector.getCompanyImage(store),
      getCompanyByApi: MainPageSelector.getCompanyesFromApi(store),
    );
  }
}
