import 'dart:collection';

import 'package:redux_state_practice/models/company.dart';
import 'package:redux_state_practice/store/app/reducer.dart';
import 'package:redux_state_practice/store/shared/main_page_state/main_page_actions.dart';

class MainPageState {
  final List<Company> company;
  final List<String> companyImages;
  MainPageState({
    this.company,
    this.companyImages,
  });

  factory MainPageState.initial() {
    return MainPageState(
      company: [],
      companyImages: [],
    );
  }
  MainPageState copyWith({List<Company> company, List<String> companyImages}) {
    return MainPageState(
      company: company ?? this.company,
      companyImages: companyImages ?? this.companyImages,
    );
  }

  MainPageState reducer(dynamic action) {
    return Reducer<MainPageState>(
      actions: HashMap.from(
        {
          SetCompany: (action) => _setList(action.companies),
          SetCompanyImage: (action) => _setImages(action.images)
        },
      ),
    ).updateState(action, this);
  }

  MainPageState _setList(List<Company> newlist) {
    print(newlist.length);
    return copyWith(company: newlist);
  }

  MainPageState _setImages(List<String> newlist) {
    print(newlist.length);
    print(newlist);
    return copyWith(companyImages: newlist);
  }
}
