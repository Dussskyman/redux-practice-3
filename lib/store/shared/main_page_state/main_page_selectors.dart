import 'package:redux/redux.dart';
import 'package:redux_state_practice/models/company.dart';
import 'package:redux_state_practice/store/app/app_state.dart';
import 'package:redux_state_practice/store/shared/main_page_state/main_page_actions.dart';

class MainPageSelector {
  static List<Company> getCompany(Store<AppState> store) {
    return store.state.mainPageState.company;
  }

  static List<String> getCompanyImage(Store<AppState> store) {
    return store.state.mainPageState.companyImages;
  }

  static Function(List companies) setCompany(Store<AppState> store) {
    return (List companies) => store.dispatch(SetCompany(companies));
  }

  static Function(List images) setCompanyImage(Store<AppState> store) {
    return (List images) => store.dispatch(SetCompanyImage(images));
  }

  static Function() getCompanyesFromApi(Store<AppState> store) {
    return () => store.dispatch(GetCompanyByApi());
  }
}
