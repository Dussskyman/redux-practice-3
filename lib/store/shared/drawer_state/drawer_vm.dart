import 'package:flutter/widgets.dart';
import 'package:redux/redux.dart';
import 'package:redux_state_practice/store/app/app_state.dart';
import 'package:redux_state_practice/store/shared/drawer_state/drawer_selector.dart';

class DrawerViewModel {
  final Color color;
  final void Function(Color color) setColor;

  DrawerViewModel({
    @required this.color,
    @required this.setColor,
  });

  static DrawerViewModel fromStore(Store<AppState> store) {
    return DrawerViewModel(
      color: DrawerSelector.getColor(store),
      setColor: DrawerSelector.setColor(store),
    );
  }
}
