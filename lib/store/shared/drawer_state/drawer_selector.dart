import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_state_practice/store/app/app_state.dart';
import 'package:redux_state_practice/store/shared/drawer_state/drawer_actions.dart';

class DrawerSelector {
  static Color getColor(Store<AppState> store) {
    return store.state.drawerState.color;
  }

  static void Function(Color) setColor(Store<AppState> store) {
    return (color) => store.dispatch(SetColor(color));
  }
}
