import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:redux_state_practice/store/app/reducer.dart';
import 'package:redux_state_practice/store/shared/drawer_state/drawer_actions.dart';

class DrawerState {
  final Color color;
  DrawerState({this.color});

  factory DrawerState.initial() {
    return DrawerState(color: Colors.green);
  }
  DrawerState copyWith({Color color}) {
    return DrawerState(color: color ?? this.color);
  }

  DrawerState reducer(dynamic action) {
    return Reducer<DrawerState>(
      actions: HashMap.from(
        {
          SetColor: (dynamic action) => _setColor(action.color),
        },
      ),
    ).updateState(action, this);
  }

  DrawerState _setColor(Color newcolor) {
    print('$newcolor');
    return copyWith(color: newcolor);
  }
}
