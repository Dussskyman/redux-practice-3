import 'package:redux_epics/redux_epics.dart';
import 'package:redux_state_practice/store/shared/drawer_state/drawer_state.dart';
import 'package:redux_state_practice/store/shared/main_page_state/main_page_epics.dart';
import 'package:redux_state_practice/store/shared/main_page_state/main_page_state.dart';

class AppState {
  final DrawerState drawerState;
  final MainPageState mainPageState;
  AppState({
    this.drawerState,
    this.mainPageState,
  });

  factory AppState.initial() {
    return AppState(
      drawerState: DrawerState.initial(),
      mainPageState: MainPageState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState(
      drawerState: state.drawerState.reducer(action),
      mainPageState: state.mainPageState.reducer(action),
    );
  }

  static final getAppEpic = combineEpics([MainPageEpics.indexEpic]);
}
