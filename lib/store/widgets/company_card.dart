import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_state_practice/const/routes.dart';
import 'package:redux_state_practice/models/company.dart';
import 'package:redux_state_practice/store/app/app_state.dart';
import 'package:redux_state_practice/store/shared/navigation_vm.dart';

class CompanyCard extends StatefulWidget {
  final Company company;
  final String image;
  const CompanyCard({Key key, this.company, this.image}) : super(key: key);

  @override
  _CompanyCardState createState() => _CompanyCardState();
}

class _CompanyCardState extends State<CompanyCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: StoreConnector<AppState, NavigationVM>(
        converter: (store) => NavigationVM.fromStore(store),
        builder: (context, vm) => InkWell(
          onTap: () {
            print('something');
            vm.changePage(Routes.second, widget.company);
          },
          child: SizedBox(
            width: double.infinity,
            child: Card(
              elevation: 5,
              child: DecoratedBox(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    gradient: LinearGradient(
                        colors: [Colors.deepOrange, Colors.black])),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            widget.company.name,
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      SingleChildScrollView(
                        physics: NeverScrollableScrollPhysics(),
                        child: Column(
                          children: [
                            Text(
                              'City: ${widget.company.city}',
                              style:
                                  TextStyle(fontSize: 16, color: Colors.white),
                            ),
                            Text(
                              'State: ${widget.company.state}',
                              style:
                                  TextStyle(fontSize: 14, color: Colors.white),
                            ),
                            Text(
                              'Country: ${widget.company.country}',
                              style:
                                  TextStyle(fontSize: 14, color: Colors.white),
                            ),
                            widget.company.street != null
                                ? Text(
                                    'Adress: ${widget.company.street}',
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.white),
                                  )
                                : SizedBox(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
