import 'package:flutter/material.dart';
import 'package:redux_state_practice/models/company.dart';

class SecondPage extends StatelessWidget {
  final Company company;

  const SecondPage({Key key, this.company}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DecoratedBox(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Colors.white,
              Colors.grey,
            ],
          ),
        ),
        child: SafeArea(
          child: Center(
            child: Column(
              children: [
                Text(
                  company.name,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 60),
                ),
                Text(
                  'State: ' + company.state,
                  style: TextStyle(fontSize: 30),
                ),
                Text(
                  'City: ' + company.city,
                  style: TextStyle(fontSize: 30),
                ),
                Text(
                  'Country: ' + company.country,
                  style: TextStyle(fontSize: 30),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
