import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_state_practice/store/app/app_state.dart';
import 'package:redux_state_practice/store/shared/drawer_state/drawer_vm.dart';
import 'package:redux_state_practice/store/shared/main_page_state/main_page_vm.dart';
import 'package:redux_state_practice/store/widgets/company_card.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Color> colors = [
    Colors.red,
    Colors.green,
    Colors.blue,
    Colors.yellow,
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: StoreConnector<AppState, DrawerViewModel>(
        converter: (store) => DrawerViewModel.fromStore(store),
        builder: (context, vm) => Drawer(
          child: Scaffold(
            backgroundColor: vm.color,
            body: ListView.builder(
              itemCount: 4,
              itemBuilder: (context, index) => Container(
                width: double.infinity,
                height: 100,
                color: colors[index],
                child: TextButton(
                  onPressed: () => vm.setColor(colors[index]),
                  child: Text('$index',
                      style: TextStyle(fontSize: 30, color: Colors.black)),
                ),
              ),
            ),
          ),
        ),
      ),
      body: Center(
        child: StoreConnector<AppState, MainPageVM>(
            converter: MainPageVM.fromStore,
            onInitialBuild: (viewModel) => viewModel.getCompanyByApi(),
            builder: (context, vm) {
              if (vm.images.isEmpty) {
                return CircularProgressIndicator();
              }
              return ListView.builder(
                itemCount: vm.companies.length,
                itemBuilder: (context, index) => CompanyCard(
                  company: vm.companies[index],
                  image: vm.images[index],
                ),
              );
            }),
      ),
    );
  }
}
