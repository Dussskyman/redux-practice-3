import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux_state_practice/const/routes.dart';
import 'package:redux_state_practice/pages/main_page.dart';
import 'package:redux_state_practice/pages/second_page.dart';
import 'package:redux_state_practice/store/app/app_state.dart';

void main() {
  Store store = Store<AppState>(AppState.getAppReducer,
      initialState: AppState.initial(),
      middleware: [
        EpicMiddleware(AppState.getAppEpic),
        NavigationMiddleware<AppState>()
      ]);
  runApp(MyApp(store: store));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  const MyApp({this.store});
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        navigatorKey: NavigatorHolder.navigatorKey,
        onGenerateRoute: (settings) {
          switch (settings.name) {
            case (Routes.main):
              return MaterialPageRoute(
                builder: (context) => MyHomePage(),
              );
            case (Routes.second):
              return MaterialPageRoute(
                builder: (context) => SecondPage(
                  company: settings.arguments,
                ),
              );
          }
        },
        home: MyHomePage(),
      ),
    );
  }
}
