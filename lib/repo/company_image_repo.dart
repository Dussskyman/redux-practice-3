import 'package:redux_state_practice/models/company.dart';
import 'package:redux_state_practice/service/company_service.dart';

class CompanyImageRepo {
  CompanyImageRepo._();
  static final CompanyImageRepo instance = CompanyImageRepo._();

  Future<List<String>> apiResponce() async {
    return CompanyServise.instance.apiImageResponce().then(
          (value) => value.map(
            (e) {
              print(e);
              return e['download_url'] as String;
            },
          ).toList(),
        );
  }
}
