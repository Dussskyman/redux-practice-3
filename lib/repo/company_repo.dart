import 'package:redux_state_practice/models/company.dart';
import 'package:redux_state_practice/service/company_service.dart';

class CompanyRepo {
  CompanyRepo._();
  static final CompanyRepo instance = CompanyRepo._();

  Future<List<Company>> apiResponce() async {
    return CompanyServise.instance.apiResponce().then(
          (value) => value
              .map(
                (e) => Company.fromJson(e),
              )
              .toList(),
        );
  }
}
