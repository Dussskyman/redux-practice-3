import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

class Company {
  final int id;
  final String obdbId;
  final String name;
  final String breweryType;
  final String street;
  final String city;
  final String state;
  final String country;

  Company({
    @required this.id,
    @required this.obdbId,
    @required this.name,
    @required this.breweryType,
    @required this.street,
    @required this.city,
    @required this.state,
    @required this.country,
  });
  factory Company.fromJson(
    Map json,
  ) {
    return Company(
      id: json['id'],
      obdbId: json['obdb_id'],
      name: json['name'],
      breweryType: json['brewery_type'],
      street: json['street'],
      city: json['city'],
      state: json['state'],
      country: json['country'],
    );
  }
}
