import 'dart:convert';

import 'package:http/http.dart' as http;

class CompanyServise {
  CompanyServise._();
  static final CompanyServise instance = CompanyServise._();

  Future<List<dynamic>> apiResponce() async {
    return await http
        .get('https://api.openbrewerydb.org/breweries')
        .then((value) => jsonDecode(value.body) as List<dynamic>);
  }

  Future<List<dynamic>> apiImageResponce() async {
    return await http
        .get('https://picsum.photos/v2/list')
        .then((value) => jsonDecode(value.body) as List<dynamic>);
  }
}
